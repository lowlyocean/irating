const express = require('express');
const router = express.Router();
const rateLimit = require('express-rate-limit'); // Limits allowed calls for x amount of ms
const slowDown = require('express-slow-down'); // Slows each following request if spammed
const axios = require('axios'); // Use axios to make http requests
// const example = require('./example.js'); ==> load specifc api file

const SSE = require('express-sse');
const sse = new SSE();

var irsdk = require('node-irsdk');

irsdk.init({
  telemetryUpdateInterval: 1000,
  sessionInfoUpdateInterval: 1000
})

var iracing = irsdk.getInstance()

var iRating = {};
var custID = null;
var names = {}
iracing.on('Connected', function () {
	console.log('\nConnected to iRacing.')

	iracing.once('Disconnected', function () {
		console.log('iRacing shut down.')
	})

	iracing.on('SessionInfo', function (sessionInfo) {
		console.log('SessionInfo event received\n')
		if(!sessionInfo.data.SessionInfo.Sessions.some( session => session.SessionType == "Race" ) )
		{
			console.error('No race session found.');
			return;
		}
		iRating = {};
		custID = null;
		names = {}
		custID = sessionInfo.data.DriverInfo.DriverUserID;
		var userClass = sessionInfo.data.DriverInfo.Drivers.find(drv => drv.UserID == custID).CarClassShortName;
		sessionInfo.data.DriverInfo.Drivers.filter(drv => drv.CarClassShortName == userClass).forEach(function (driver) {
			iRating[driver.UserID.toString()] = driver.IRating;
			names[driver.UserID.toString()] = driver.UserName;
		});
		sse.send({iRmap: iRating, names, custID}, 'RaceSessionInfo');
	})
})

// Limiting number of requests possible for a set amount of time
const limiter = rateLimit({
	windowMs: 30 * 1000, // Time frame (30 seconds * 1000ms)
	max: 10, // Max amount of requests
});

// Slowing down each request after a specific number of requests
const speedLimiter = slowDown({
	windowMs: 30 * 1000, // Time frame (30 seconds * 1000ms)
	delayAfter: 3, // Start delaying after 3 requests within the time frame
	delayMs: 500 // Make each requests 500ms slower
});

router.get('/sse', sse.init);

// router.use('/example-path', example); ==> to be routed to: api/chosen_path for example

module.exports = router;