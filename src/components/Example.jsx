import React, {Fragment} from 'react';

import './Example.css';

const es = new EventSource('http://localhost:3000/api/sse');

const denom = 1600 / Math.log(2);
function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    }

    if (typeof step == 'undefined') {
        step = 1;
    }

    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) {
        return [];
    }

    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }

    return result;
};

const score = (pos, compet, scenario) => {
	const posiR = scenario.find(placement => placement.place == pos).iR;
	const competiR = scenario.find(placement => placement.place == compet).iR;
    return ((1 - Math.exp(-posiR / denom)) * (Math.exp(-competiR / denom))) / (
            (1 - Math.exp(-competiR / denom)) * (Math.exp(-posiR / denom)) + (1 - Math.exp(-posiR / denom)) * (
				Math.exp(-competiR / denom)));
}

const fudge = (place, scenario) => {
    return ((scenario.length) / 2 - place) / 100
}

const ir_Delta = (place, scenario) => {
    return ((scenario.length - place - (range(1, scenario.length + 1).map(c => score(place, c, scenario)).reduce((a,b) => a+b) - 0.5)) - fudge(place, scenario)) * 200 / scenario.length
}

const zip = (arr, ...args) =>
  arr.map((value, idx) => [value, ...args.map(arr => arr[idx])])

export default class Example extends React.Component {

	state = {
		iRmap: null,
		custID: null
	}

	sseHandler = (event) => {
		const {custID, iRmap, names} = JSON.parse(event.data);
		delete iRmap["-1"];
		delete names["-1"];
		this.setState({iRmap, custID, names});
	}

	componentDidMount() {
		es.addEventListener('RaceSessionInfo', this.sseHandler);
	}

	componentWillUnmount() {
		es.removeEventListener('RaceSessionInfo', this.sseHandler);
	}

	build_scenario = (finPos,iRmap, custID) => {
		const positions = range(1, Object.entries(iRmap).length + 1).filter(p => p != finPos);
		const iRs = Object.entries(iRmap).sort(([,a],[,b]) => b-a).filter(([cID, _iR]) => parseInt(cID) != custID).map(([_cID, iR]) => iR);
		const scenario = [...[{'place': finPos, 'iR': iRmap[custID]}], 
			...zip(positions, iRs).map(([place,iR]) => {return {place, iR};})
		];
		return scenario;
	}
	
	render() {
		const {iRmap, custID, names} = this.state;
		return (
			<Fragment>
				<table><thead>
					<tr><th>Position</th><th>Name</th><th>iRating</th><th>Change</th></tr>
				</thead>
				<tbody>{iRmap &&
					Object.entries(iRmap).sort(([,a],[,b]) => b-a).map(([key, value], i) => {
							return <tr key={key}><td>{i+1}</td><td>{names[key]}</td><td>{value}</td><td>{Math.round(ir_Delta(i+1,this.build_scenario(i+1,iRmap, custID)))}</td></tr>
						})
				}</tbody>
				</table>
			</Fragment>
		);
	}
}