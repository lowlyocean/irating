// webpack needs to be explicitly required
const webpack = require('webpack')

module.exports = {
	entry: __dirname+'/src/App.jsx',
	target: 'node',
	mode: 'production',
	output: {
		path: __dirname+'/public/js',
		filename: 'bundle.js'
	},
	watch: false,
	node: {
		__dirname: true
	},
	module: {
		rules: [
			{
				test: /\.css$/i,
				use: ["style-loader", "css-loader"],
			},
			{
				test: /\.node$/,
				loader: "node-loader",
			},
			{
				test: /\.jsx$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [
							['@babel/preset-env'],
							['@babel/preset-react']
						],
						plugins: [
						  "@babel/plugin-proposal-class-properties",
						  "@babel/transform-runtime"
						]
					}
				}
			}
		]
	}
}